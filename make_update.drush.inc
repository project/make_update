<?php

/**
 * @file
 * Make update drush include file.
 */

/**
 * Implements hook_drush_command().
 */
function make_update_drush_command() {
  $items['make-update'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => 'Updates a makefile.',
    'arguments' => array(
      'makefile' => 'Filename of the makefile to use for this build.',
    ),
    'engines' => array(
      'version_control',
      'package_handler',
      'release_info' => array('add-options-to-command' => FALSE),
    ),
  );

  return $items;
}

/**
 * @param string $path
 * @return bool
 */
function drush_make_update($path = 'drush.make') {
  drush_include_engine('update_info', 'drupal', NULL, DRUSH_BASE_PATH . '/commands/pm/update_info');
  include_once DRUSH_BASE_PATH . '/commands/pm/updatecode.pm.inc';
  include_once DRUSH_BASE_PATH . '/commands/make/generate.make.inc';

  $makefile = realpath(drush_cwd() . '/' . $path);
  $info = make_parse_info_file($makefile);

  if ($info === FALSE || ($info = make_validate_info_file($info)) === FALSE) {
    return FALSE;
  }

  $projects = array();
  foreach ($info['projects'] as $key => $project) {
    if (isset($project['download']) && isset($project['download']['type']) && $project['download']['type'] === 'git' && isset($project['download']['revision'])) {
      // Do not include custom modules with a specific git revision.
      continue;
    }

    $project_info = $project + array(
      'name'                => $key,
      'core'                => $info['core'],
      'translations'        => array(),
      'build_path'          => '',
      'contrib_destination' => 'sites/all/modules',
      'version'             => '',
      'location'            => RELEASE_INFO_DEFAULT_URL,
      'subdir'              => '',
      'directory_name'      => '',
    );

    $request = make_prepare_request($project_info);
    $xml = updatexml_get_release_history_xml($request);
    $update_info = updatexml_get_releases_from_xml($xml, $key);

    $new_version = $update_info['recommended'];

    if ($new_version === $project_info['version']) {
      continue;
    }

    $new_version = str_replace($project_info['core'] . '-', '', $new_version);

    if ($new_version === $project_info['version']) {
      continue;
    }

    $project['version'] = $new_version;
    $project['_type'] = 'core';

    $projects[$key] = $project;
  }

  if (empty($projects)) {
    drush_print(t('Nothing to update!'));
  }
  $make = _drush_make_generate_makefile_body($projects);
  drush_print($make);
}
